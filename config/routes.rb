Log::Tracker::Engine.routes.draw do
  resources :apps
  resources :problems do
    put :resolve
  end
end
