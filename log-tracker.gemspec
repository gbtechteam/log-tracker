$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "log/tracker/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "log-tracker"
  s.version     = Log::Tracker::VERSION
  s.authors     = ["Mihir"]
  s.email       = ["mihir@geobeats.com"]
  s.homepage    = "http://soulcurry.co"
  s.summary     = "Summary of Log::Tracker."
  s.description = "Description of Log::Tracker."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.required_ruby_version = '>= 2.1.0'
  s.add_dependency("rails", ">= 4.1.0", "< 5.2")

  s.add_development_dependency "sqlite3"
end
