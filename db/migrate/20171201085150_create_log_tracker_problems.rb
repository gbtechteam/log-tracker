class CreateLogTrackerProblems < ActiveRecord::Migration[5.1]
  def change
    create_table :log_tracker_problems do |t|
      t.datetime :last_notice_at, default: Time.now
      t.datetime :first_notice_at, default: Time.now
      t.datetime :resolved_at
      t.boolean  :resolved, default: false
      t.integer  :app_id
      t.integer  :notices_count, default: 0
      t.string   :issue_link
      t.string   :issue_type
      t.string   :environment
      t.string   :where
      t.text     :messages
      t.text     :user_agents
      t.text     :hosts

      t.timestamps
    end
  end
end
