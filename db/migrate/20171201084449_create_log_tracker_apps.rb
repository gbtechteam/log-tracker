class CreateLogTrackerApps < ActiveRecord::Migration[5.1]
  def change
    create_table :log_tracker_apps do |t|
      t.string :name

      t.timestamps
    end
  end
end
