module Log
  module Tracker
    class Engine < ::Rails::Engine
      isolate_namespace Log::Tracker
    end
  end
end
