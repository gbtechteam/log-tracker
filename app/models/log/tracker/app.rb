module Log::Tracker
  class App < ApplicationRecord
    has_many :problems, inverse_of: :app, dependent: :destroy

    validates :name, presence: true, uniqueness: { allow_blank: true }
  end
end
