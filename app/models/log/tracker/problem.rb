module Log::Tracker
  class Problem < ApplicationRecord
    DEFAULT_APP_ID = 1

    belongs_to :app

    validates :environment, presence: true
    validates :last_notice_at, :first_notice_at, presence: true

    def self.notify(options = {})
      app_id = options[:app_id] || DEFAULT_APP_ID

      App.find(app_id).problems.create!(
        environment: Rails.env,
        messages: options[:messages],
        issue_type: options[:issue_type],
        where: options[:where]
      )
    end
  end
end
