module Log
  module Tracker
    module ApplicationHelper

      def button_class(resolved)
        resolved ? "btn-success" : "btn-danger"
      end
    end
  end
end
