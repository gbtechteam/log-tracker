require_dependency "log/tracker/application_controller"

module Log::Tracker
  class AppsController < ApplicationController
    def index
      @apps = Log::Tracker::App.all
    end
  end
end
