require_dependency "log/tracker/application_controller"

module Log::Tracker
  class ProblemsController < ApplicationController

    def index
      @problems = Problem.all
    end

    def resolve
      problem = Problem.find(params[:problem_id])
      problem.resolved = true
      problem.touch(:resolved_at)
      problem.save
    end
  end
end
